---
layout: post
categories: events
title: "Guest lecture: How to become an open source systems engineer?"
date: 2019-03-2
author: siqueira
redirect_from: /events/2019/03/02/how-to-became-open-source-engineering
lang: en
excerpt_separator: <!--end-abstract-->
---

It is with great joy that we continue the cycle of professional lectures
presented by several professionals who work with free software on a daily
basis. This time we will have Lucas Kanashiro from Collabora.

<!--end-abstract-->

We're glad to have [Collabora](https://www.collabora.com/)'s open source system
engineering, Lucas Kanashiro, with the lecture "**How to become an open source
systems engineer?**".

|**When**  | Friday, March 8, 2019, at 2pm |
|**Where** | IME-USP, CCSL, CCSL Auditorium (downstairs) |

#### About the speaker

Lucas Kanashiro is a software engineer at Collabora, currently working on
open source systems integration. Lucas is a Debian Developer and contributes
with free software in general. Also, he hopes to be a former master's student
in computer science at IME / USP soon :)

#### About the lecture

Joining free software communities is not an easy task at first glance, you
have to learn about the community's behavior, understand the software you want
to contribute, and have to insert yourself into the community workflow; i.e.,
there are many challenges. Knowing all of this issue, Lucas Kanashiro will
share his journey from academia, passing through his involvement as an active
member of the Debian project and ending with his current position as an open
source systems engineer. Finally, he will discuss a little about the last
challenges in his short journey as a software engineer at Collabora.

*Note: The lecture will be presented in portuguese.*

