---
layout: post
categories: events
title: "Guest lecture: 3 Days in the Life of a Toolchain Engineer"
redirect_from: /events/2019/02/01/lecture-3-days-in-the-life-of-a-toolchain-engineer
author: matheustavares
lang: en
excerpt_separator: <!--end-abstract-->
---

It is with great joy that we begin a cycle of professional lectures presented by
several professionals who work with free software on a daily basis. This cycle
will take place throughout the year according to the availability of the
speakers.

<!--end-abstract-->

In particular, to open our lecture series, we will be honored to have
[SUSE](https://www.suse.com/)'s toolchain engineer, João Moreira, with the
lecture "**3 Days in the Life of a Toolchain Engineer**".

|**When**  | Tuesday, february 5, 2019, at 2pm |
|**Where** | IME-USP, room B16 (B building)   |

#### About the speaker
João Moreira currently works for SUSE as a toolchain engineer at SUSE Labs.
João is a PhD from Unicamp, working with Control-Flow Integrity models for the
Linux kernel. Previously, he gave lectures at conferences such as Linuxdev-br,
Black Hat Asia, H2HC, EkoParty, and FISL.

#### About the lecture
In this lecture, João brings the theoretical, practical and technical
perspective of three of his activities as an engineer at Suse, showing a
panorama about the actuation of a toolchain engineer. In addition, given the
speaker's various areas of activity, at the end of the presentation, João will
open a space for discussions about free software, live-patching, and other
related topics.

*Note: The lecture will be presented in portuguese.*

You can watch it [here](https://www.youtube.com/watch?v=k5AfwiKVXJc).

